# sensorsio

`sensorsio` is a python library provides convenient functions to load Sentinel2 and other sensors data into `numpy` and `xarray`.

## Quickstart

### Reading a Sentinel2 product

Reading a **Sentinel2 L2A** product is as simple as:

```python
# Import the sentinel 2 module
from sensorsio import sentinel2
# Create an instance of Sentinel2 class from the product path
dataset = sentinel2.Sentinel2('/datalake/S2-L2A-THEIA/31TDH/2019/05/31/SENTINEL2B_20190531-105916-927_L2A_T31TDH_C_V2-2/')
# Read bands, masks and coords to numpy
bands, masks, xcoords, ycoords, crs = dataset.read_as_numpy(sentinel2.Sentinel2.GROUP_10M)
# Read bands, masks and coords to xarray
xrds = dataset.read_as_xarray(sentinel2.Sentinel2.GROUP_10M)
print(xrds)
```
```
<xarray.Dataset>
Dimensions:  (t: 1, x: 10980, y: 10980)
Coordinates:
  * t        (t) datetime64[ns] 2019-05-31
  * x        (x) float64 4e+05 4e+05 4e+05 ... 5.097e+05 5.097e+05 5.098e+05
  * y        (y) float64 4.8e+06 4.8e+06 4.8e+06 ... 4.69e+06 4.69e+06 4.69e+06
Data variables:
    B2       (t, y, x) float32 0.0364 0.0378 0.0406 0.0393 ... nan nan nan nan
    SAT      (t, y, x) uint8 0 0 0 0 0 0 0 0 0 0 0 0 ... 0 0 0 0 0 0 0 0 0 0 0 0
    CLM      (t, y, x) uint8 0 0 0 0 0 0 0 0 0 0 0 0 ... 0 0 0 0 0 0 0 0 0 0 0 0
    EDG      (t, y, x) uint8 0 0 0 0 0 0 0 0 0 0 0 0 ... 1 1 1 1 1 1 1 1 1 1 1 1
    MG2      (t, y, x) uint8 0 0 0 0 0 0 0 0 0 0 0 0 ... 0 0 0 0 0 0 0 0 0 0 0 0
    B3       (t, y, x) float32 0.0652 0.068 0.0742 0.0752 ... nan nan nan nan
    B4       (t, y, x) float32 0.063 0.0712 0.0735 0.0728 ... nan nan nan nan
    B8       (t, y, x) float32 0.2343 0.2349 0.2369 0.2456 ... nan nan nan nan
Attributes:
    tile:     31TDH
    type:     FRE
    crs:      EPSG:32631
```
### Jointly reading multiple sensors on a common grid

It is also very simple to reproject several images from different sensors to a common grid for manipulation:

```python
# Create a Sentinel2 dataset
s2_ds = sentinel2.Sentinel2(s2)
# Create a Pleiades dataset
phr_ds = pleiades.Pleiades(phr_xs)
# Find common grid
box, crs = utils.bb_common([s2_ds.bounds, phr_ds.bounds],[s2_ds.crs, phr_ds.crs],snap=10)
# Warp Sentinel2 on this grid:
s2_arr, _, _, _, _ = s2_ds.read_as_numpy(sentinel2.Sentinel2.GROUP_10M,
                                         resolution=10,
                                         crs=crs,
                                         bounds=box)
# Warp Pléiades on this grid:
phr_arr, _, _, _ = phr_ds.read_as_numpy(pleiades.Pleiades.GROUP_XS,
                                        resolution=10,
                                        crs=crs,
                                        bounds=box,
                                        algorithm=rio.enums.Resampling.cubic)
print(phr_arr.shape, s2_arr.shape)
```
```
((4, 5774, 2082), (4, 5774, 2082))
```

This is demonstrated in more details in [this notebook](notebooks/joint_sensors.ipynb).

### Visualisation made easy

`sensorsio` also contains utilities to prepare `numpy` arrays for visualisation with `matplotlib`:

```python
# Prepare for rendering (band extraction and scaling)
dmin = np.array([0., 0., 0.])
dmax = np.array([0.2,0.2,0.2])
s2_rgb, dmin, dmax = utils.rgb_render(s2_arr, bands=[2,1,0], 
                                      dmin=dmin, 
                                      dmax=dmax)
phr_rgb, dmin, max = utils.rgb_render(phr_arr, bands=[2,1,0], 
                                      dmin=dmin, 
                                      dmax=dmax)
 # Call matplotlib
 fig, axes = plt.subplots(ncols=2, figsize=(int(25*phr_rgb.shape[1]/phr_rgb.shape[0]), 25))
axes[0].imshow(s2_rgb)
axes[0].set_title(str(s2_ds.satellite.value))
axes[1].imshow(phr_rgb)
axes[1].set_title(str(phr_ds.satellite.value))
fig.show()
 ```
 
 
## Available drivers
### Sentinel2 L2A (Theia)

For **Sentinel2 L2A** products from Theia, it offers:
*  Convenient attributes like day of year or sensor id
*  Selective read of desired bands and masks
*  On-the-fly resampling of 20m bands to 10m while reading
*  On-the-fly projection to a different Coordinates Reference System while reading
*  Image and geographical spatial subsetting
*  Supports registration offsets computed by StackReg
*  Access to solar and view angles

See [this notebook](notebooks/sentinel2.ipynb) for an in depth review of the capabilties with ```Sentinel2``` class.

See [this notebook](notebooks/sentinel2_angles.ipynb) for the access to solar and view angles.

### Venus L2A (Theia)

For **Venus L2A** products from Theia, it offers:
*  Convenient attributes like day of year
*  Selective read of desired bands and masks
*  On-the-fly projection to a different Coordinates Reference System while reading
*  Image and geographical spatial subsetting

See [this notebook](notebooks/venus.ipynb) for an in depth review of the capabilties with ```Venus``` class.


## Installation

Pass the path to cloned repository to ```pip install```:
```bash
$ pip install sensorsio
```
## TODO list

- [x] Add Water Vapor and AOT bands to sentinel2 driver (in progress)
- [x] Add solar and satellite angles to sentinel2 driver (in progress)
- [x] Add relative orbit number computation to sentinel2 driver
- [x] Add footprint polygon to sentinel2 driver (intersection of MGRS tile and orbit swath)

## Notes

This project has been set up using PyScaffold 4.0.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
